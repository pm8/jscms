# jsCMS
## What is this?
jsCMS is a Contents Management System for Node.js with Express4 and MongoDB.
and render by json file.

## Required
```
jscms /Volumes/DATA1/git/jsCMS
├── body-parser
├── express
├── express-generator
├── method-override
├── mongodb
├── morgan
├── node-sass
├── node-zip
├── pug
├── riot
└── uglify-js
```

## install
### CentOS 7
#### install nodeJS

```
curl --silent --location https://rpm.nodesource.com/setup_6.x | bash -
yum -y install nodejs
```

https://nodejs.org/en/download/package-manager/

#### clone project

```
cd /path/to/put/project
git clone https://bitbucket.org/pm8/jscms.git
```

#### install required files
##### install from npm

```
cd /path/to/project/directory
npm i
npm install nodemon -g
```

##### MongoDB

```
vim /etc/yum.repos.d/mongodb-org-3.0.repo
```

```
[mongodb-org-3.0]
name=MongoDB Repository
baseurl=http://repo.mongodb.org/yum/redhat/$releasever/mongodb-org/3.0/x86_64/
gpgcheck=0
enabled=1
```

```
yum -y install mongodb-org
chkconfig mongod on
systemctl start mongod
```

### macOS
#### install nodeJS
visit https://nodejs.org/en/download/ and download mac installer. then install.

OR

if you have already installed homebrew, type like this

```
brew install nodebrew
nodebrew install-binary v6.9.1
```

http://brew.sh/
https://github.com/Homebrew/homebrew-core/tree/master/Formula

#### clone project

```
cd /path/to/put/project
git clone https://bitbucket.org/pm8/jscms.git
```

#### install required files
##### install from npm

```
cd /path/to/project/directory
npm i
npm install nodemon -g
```

##### MongoDB

```
vim /etc/yum.repos.d/mongodb-org-3.0.repo
```

```
[mongodb-org-3.0]
name=MongoDB Repository
baseurl=http://repo.mongodb.org/yum/redhat/$releasever/mongodb-org/3.0/x86_64/
gpgcheck=0
enabled=1
```

```
yum -y install mongodb-org
chkconfig mongod on
systemctl start mongod
```

## Directory Tree by default
- raw/
    - css/
        - (SCSS Files)
    - fragments
        - (template Files)
    - js/
        - (JavaScript Files)
    - pug/
        - (Pug Files)
    - tag/
        - (Tag Files)
- routes/
    - (node routes Files)
- static/
*NOTE: here (under the `/static`) will be document root. You can place other files such as images here if you need. <Of-course, for example, you can also place Javascript Files which won't Uglify under `lib/js` or/and CSS Files under `lib/css`.>*
    - lib/
        - css/
            - (Compiled CSS Files will place here automatically)
        - fragments/
            - (Compiled fragments Files will place here automatically)
        - js/
            - (Compiled JavaScript Files will place here automatically)
        - tag/
            - (Compiled Tag Files will place here automatically)
        - article.json *(will be Generated automatically)*
        

## Usage by default
### put raw files
put `.scss` files in `/raw/css` (NOT scss), `.js` files in `/raw/js`,and `.pug` files in `/raw/pug`.

### start project

```
cd /path/to/project/directory
nodemon server.js
```

project open localhost:80 by default.
if you need to change something, edit `/setting.js`.


### mongo
```
mkdir -p /data/db
mongo
> use <db name>
> db.createCollection('<collection name>')
```


## functions
### pug
article list will generate under (ONLY FIRST) `[data-ctl="list"]`  element.