/**! @license server.js; JavaScript for jsCMS by Yuki TANABE on Oct 23, 2016 | pm8.jp */

const express = require('express');
const app = express();
const logger = require('morgan');
const methodOverride = require('method-override');
const bodyParser = require('body-parser');

const settings = require('./settings.js');
const routes = settings.routes;
const post = require(routes['post']);
const mongo = require(routes['mongo']);
const get = require(routes['get']);

app.set('view engine', 'pug');
app.set('views', settings.rawPath['pug']);

app.locals.pretty = false; //output one-line html from pug file when false.


//middleware
app.use(logger('dev')); //logging for dev
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(methodOverride()); //middleware for such as POST, PUT and DELETE.
app.use(express.static(settings.path['static'])); //find static files with directory and file name(s).

////get
app.get(settings.URL['action'], get.action);
app.get(settings.URL['index'], get.index);
app.get(settings.URL['admin'], get.admin);
app.get(settings.URL['sandbox'], get.sandbox);
app.get(settings.URL['tmpJson'], get.json);
app.get(settings.URL['prmJson'], get.json);


////post
app.post('/action', post.action);
app.post('/create', post.create);
app.post('/edit', post.edit);
app.post('/remove', post.remove);


app.use(function(req, res, next){ //404 error
	res.status(404);
	res.render(settings.pug['notfound']);
});
app.use(function(err, req, res, next){ //500 error
	res.status(500);
	res.render(settings.pug['servererr'],{err:err});
});

app.listen(settings.port);
console.log('start nodeServer');