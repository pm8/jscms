/**! @license post.js; JavaScript for jsCMS by Yuki TANABE on Oct 24, 2016 | pm8.jp */

const settings = require('../settings.js');
const mongo = require(settings.routes['mongo']);

exports.action=function(req,res){
	res.redirect(307, '/'+req.body.action);
};
exports.create=function (req,res) {
	var title =req.body.title;
	var body = req.body.body;
	var time = Date.now();
	var data = {title:title, body:body,time:time, update:time};
	if(title&&body){
		mongo.insert2Mongo([data]);
	}
	res.redirect(settings.URL['admin']);
};
exports.edit=function(req,res){//TODO: edit post
	console.log(req.body);
	if(req.body.id){
		mongo.editRecord(req.body);
	}
	res.redirect(settings.URL['admin']);
};
exports.remove=function(req,res){
	var t='';
	if(req.body.id){
		console.log('removing from id: '+req.body.id);
		mongo.removeRecord(req.body.id);
	}
	res.redirect(settings.URL['admin']);
};