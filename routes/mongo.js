/**! @license mongo.js; JavaScript for jsCMS by Yuki TANABE on Nov 6, 2016 | pm8.jp */

const mongodb = require('mongodb');
const mongoClient = mongodb.MongoClient;
const settings = require('../settings.js');
const fs = require('fs');

exports.genJson=function(req,res,route){
	var t=settings.staticPath[settings.jsonTmpFileName];
	var f=false;
	if(route===settings.URL['prmJson']){
		t=settings.staticPath[settings.jsonPrmFileName];
		f=true;
	}
	return new Promise(function(resolve, reject){
		mongoClient.connect(settings.path[settings.mongoKeyName], function (err, db) {
			if (err) {
				return console.log(err);
			}
			console.log('connected to DB named \'' + settings.db + '\'');
			getCollectionArr(db, settings.collectionName).then(function(v){
				if(f){
					for(let i=0;i<v.length; i++){
						delete v[i]._id;
					}
					console.log(v)
				}
				fs.writeFile(t, JSON.stringify(v));
				resolve();
			}).catch(function(err){
				console.log('JSON GENERATE ERROR=================================================');
				console.log(err);
				console.log('=================================================JSON GENERATE ERROR');
				reject();
			});
		});
	});
};
exports.editRecord = function(req,res){
	var t= {'_id':new mongodb.ObjectID(req.id)};
	var title=req.editTitle;
	var body =req.editBody;
	var d = new Date;
	var time = Date.UTC(d.getFullYear(), d.getMonth(), d.getDate(), d.getHours(), d.getMinutes(), d.getSeconds(), d.getMilliseconds());
	mongoClient.connect(settings.path[settings.mongoKeyName], function (err, db) {
		if (err) {
			return console.log(err);
		}
		console.log('connected to DB named \'' + settings.db + '\'');
		db.collection(settings.collectionName, function(err, collection) {
			collection.update(t,{$set:{title:title}},{upsert:false, multi:false},function(err){
				if(err){
					console.log('update failed: '+err);
				}else{
					console.log('success to update.');
				}
			});
			collection.update(t,{$set:{body:body}},{upsert:false, multi:false},function(err){
				if(err){
					console.log('update failed: '+err);
				}else{
					console.log('success to update.');
				}
			});
			collection.update(t,{$set:{update:time}},{upsert:false, multi:false},function(err){
				if(err){
					console.log('update failed: '+err);
				}else{
					console.log('success to update.');
				}
			});
		});
	});
};
exports.insert2Mongo=function(req){
	mongoClient.connect(settings.path[settings.mongoKeyName], function (err, db) {
		if (err) {
			return console.log(err);
		}
		console.log('connected to DB named \'' + settings.db + '\'');
		db.collection(settings.collectionName, function(err, collection){
			var articles = req;
			console.log('data is '+articles);
			collection.insert(articles, function(err, result){
				if(err){
					console.log('insert failed: '+err);
				}else{
					console.log('insert success.');
				}
			});
		});
	});
};
exports.removeRecord = function(req,res){
	var t= {'_id':new mongodb.ObjectID(req)};
	mongoClient.connect(settings.path[settings.mongoKeyName], function (err, db) {
		if (err) {
			return console.log(err);
		}
		console.log('connected to DB named \'' + settings.db + '\'');
		db.collection(settings.collectionName, function(err, collection) {
			collection.remove(t, function(err,result){
				if(err){
					console.log('remove failed: '+err);
				}else{
					console.log('success: '+result);
				}
			});
		});
	});
};
function getCollectionArr(db,collectionName){
	return new Promise(function(resolve, reject){
		var tmp=[];
		db.collection(collectionName, function (err, collection) {
			if(err){reject(err)}
			var streamObj = collection.find().stream();
			streamObj.on('data', function (item) {
				tmp.push(item);
			});
			streamObj.on('end', function () {
				resolve(tmp);
			});
		});
	});
}