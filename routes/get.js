/**! @license show.js; JavaScript for jsCMS by Yuki TANABE on Nov 6, 2016 | pm8.jp */

const settings = require('../settings.js');
const mongo = require('./mongo.js');

exports.action=function(req,res){
	res.redirect(307, '/'+req.query.action);
};
exports.index= function(req,res){
	res.render(settings.pug['index']);
};
exports.admin = function(req,res){
	mongo.genJson(req,res).then(function(){ // genJson is promised.
		res.render(settings.pug['admin']);
	});
};
exports.json = function(req,res){
	console.log(req.route.path);
	mongo.genJson(req,res,req.route.path).then(function(){ // genJson is promised.
		res.redirect(settings.URL['admin']);
	});
};
exports.sandbox = function(req,res){
	res.render(settings.pug['sandbox'],{txt:'hello!'});
};