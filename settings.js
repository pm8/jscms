/**! @license settings.js; JavaScript for jsCMS by Yuki TANABE on Oct 23, 2016 | pm8.jp */

//hostSettings
exports.port=80;
exports.host='localhost';

//mongoSettings
exports.db='testdb';
exports.mongoProtocol = 'mongodb';
exports.collectionName ='articles';

//keyName Setting, (almost) equal to each directory name
exports.pugKeyName = 'pug';
exports.routesKeyName = 'routes';
exports.rawKeyName='raw';
exports.staticKeyName = 'static';
exports.libKeyName = 'lib';
exports.cssKeyName = 'css';
exports.scssKeyName = 'scss'; // but scss directory is named 'css' by default
exports.jsKeyName = 'js';
exports.tagKeyName = 'tag';
exports.fragmentsKeyName = 'fragments';
exports.mongoKeyName = exports.mongoProtocol; // === 'mongodb' by default
exports.isAutoGen = false;
exports.fragmentsExtName='txt';

//URL settings, from '/'.
exports.URL={
	admin:'/admin',
	action:'/action',
	tmpJson:'/tmpJson',
	prmJson:'/prmJson',
	index:'/',
	sandbox:'/sandbox'
};

//jsonSettings
exports.jsonPrmFileName = exports.collectionName; // === 'articles' by default
exports.jsonTmpFileName = exports.collectionName+'_tmp'; // === 'articles_tmp' by default

//maps
exports.path={};
exports.rawPath={};
exports.staticPath={};
exports.path[exports.rawKeyName]=__dirname+'/'+exports.rawKeyName;
exports.path[exports.routesKeyName]=__dirname+'/'+exports.routesKeyName;
exports.path[exports.staticKeyName]=__dirname+'/'+exports.staticKeyName;
exports.path[exports.mongoKeyName]=exports.mongoProtocol+'://'+ exports.host + '/' + exports.db;
exports.rawPath[exports.pugKeyName]=exports.path[exports.rawKeyName]+'/'+exports.pugKeyName;
exports.rawPath[exports.scssKeyName]=exports.path[exports.rawKeyName]+'/'+exports.cssKeyName; // scss directory name is 'css' by default.
//exports.rawPath[exports.scssKeyName]=exports.path[exports.rawKeyName]+'/'+exports.scssKeyName; // enable this line and disable prev line if you use scss directory named 'scss'.
exports.rawPath[exports.tagKeyName]=exports.path[exports.rawKeyName]+'/'+exports.tagKeyName;
exports.rawPath[exports.jsKeyName]=exports.path[exports.rawKeyName]+'/'+exports.jsKeyName;
exports.rawPath[exports.fragmentsKeyName]=exports.path[exports.rawKeyName]+'/'+exports.fragmentsKeyName;
exports.staticPath[exports.libKeyName]=exports.path[exports.staticKeyName]+'/'+exports.libKeyName;
exports.staticPath[exports.cssKeyName]=exports.staticPath[exports.libKeyName]+'/'+exports.cssKeyName;
exports.staticPath[exports.tagKeyName]=exports.staticPath[exports.libKeyName]+'/'+exports.tagKeyName;
exports.staticPath[exports.jsKeyName]=exports.staticPath[exports.libKeyName]+'/'+exports.jsKeyName;
exports.staticPath[exports.jsonPrmFileName]=exports.staticPath[exports.libKeyName]+'/'+exports.jsonPrmFileName+'.json';
exports.staticPath[exports.jsonTmpFileName]=exports.staticPath[exports.libKeyName]+'/'+exports.jsonTmpFileName+'.json';
exports.staticPath[exports.fragmentsKeyName]=exports.staticPath[exports.libKeyName]+'/'+exports.fragmentsKeyName;


//initialize and set more exports
const fs = require('fs');
const pug = require('pug');
const riot = require('riot');
const sass = require('node-sass');
const uglify = require("uglify-js");
const reg=/(.*)(?:\.([^.]+$))/; // for detect file names
const regURL = '.+/(.+?)\.[a-z]+([\?#;].*)?$';
const licenseRegexp = /^\!|^@preserve|^@cc_on|\bMIT\b|\bMPL\b|\bGPL\b|\(c\)|License|Copyright/mi;

var scssFiles = fs.readdirSync(exports.rawPath[exports.scssKeyName]);
for(let i=0; i<scssFiles.length; i++){
	compileScss(scssFiles[i]);
}
var rawTagFiles = fs.readdirSync(exports.rawPath[exports.tagKeyName]);
for(let i=0; i<rawTagFiles.length; i++){
	compileTag(rawTagFiles[i]);
}
var routesFiles = fs.readdirSync(exports.path[exports.routesKeyName]);
var tagFiles = fs.readdirSync(exports.staticPath[exports.tagKeyName]);
var cssFiles = fs.readdirSync(exports.staticPath[exports.cssKeyName]);
var pugFiles = fs.readdirSync(exports.rawPath[exports.pugKeyName]);
var jsFiles  = fs.readdirSync(exports.rawPath[exports.jsKeyName]);
var fragmentsFiles  = fs.readdirSync(exports.rawPath[exports.fragmentsKeyName]);

for(let i=0; i<fragmentsFiles.length;i++) {
	compilePug(exports.rawPath[exports.fragmentsKeyName] + '/'+fragmentsFiles[i], {});
}
for(let i=0; i<jsFiles.length;i++){
	var js=compressJs(exports.rawPath[exports.jsKeyName], jsFiles[i]);
	fs.writeFile(exports.staticPath[exports.jsKeyName]+'/'+jsFiles[i].match(reg)[1]+'.min.'+jsFiles[i].match(reg)[2], js.code, function (err) {
		if(err){
			console.log(err);
		}
	});
	fs.writeFile(exports.staticPath[exports.jsKeyName]+'/'+jsFiles[i].match(reg)[1]+'.min.map', js.map, function (err) {
		if(err){
			console.log(err);
		}
	});
}

var maps={};
maps[exports.scssKeyName]=scssFiles;
maps[exports.cssKeyName]=cssFiles;
maps[exports.pugKeyName]=pugFiles;
maps[exports.routesKeyName]=routesFiles;
maps[exports.tagKeyName]=tagFiles;
for(let prop in maps){
	if(!exports[prop]){
		exports[prop]={};
	}
	mapize(maps[prop],exports[prop]);
}
for(let prop in exports[exports.routesKeyName]){
	exports[exports.routesKeyName][prop]=exports.path[exports.routesKeyName]+'/'+exports[exports.routesKeyName][prop];
}
console.log(exports);


//functions
function compressJs(dir,t){
	var isLicenseComment = (function() {//check license line
		var _prevCommentLine = 0;
		return function(node, comment) {
			if (licenseRegexp.test(comment.value) || comment.line === 1 || comment.line === _prevCommentLine + 1) {
				_prevCommentLine = comment.line;
				return true;
			}
			_prevCommentLine = 0;
			return false;
		};
	})();
	return uglify.minify([dir+'/'+t], {
		compress: {
			dead_code: true,
			drop_debugger : false, // remove debugger; statements
			global_defs: {
				DEBUG: true
			},
			hoist_vars:true
		},
		output: {
			comments: isLicenseComment
		},
		outSourceMap: t.match(reg)[1]+'.min.map'
	});
}
function compilePug(t,opMap){
	var res = pug.compileFile(t);
	fs.writeFileSync(exports.staticPath[exports.fragmentsKeyName]+'/'+t.match(regURL)[1]+'.'+exports.fragmentsExtName,res(opMap));
}
function compileScss(t){
	sass.render({
		file:exports.rawPath[exports.scssKeyName]+'/'+t,
		outputStyle: 'compressed'
	},function (error,result) {
		if (error) {
			console.log('SCSS COMPILE ERROR=================================================');
			console.log(error.status);
			console.log(error.column);
			console.log(error.message);
			console.log(error.line);
			console.log('=================================================SCSS COMPILE ERROR');
		}
		else {
			fs.writeFileSync(exports.staticPath[exports.cssKeyName]+'/'+t.match(reg)[1]+'.css',result[exports.cssKeyName].toString());
		}
	});
}
function compileTag(t){
	riot.parsers[exports.cssKeyName].sass = function (tagName, css) {
		var result = sass.renderSync({
			data: css
		});
		return result[exports.cssKeyName].toString();
	};
	var code = fs.readFileSync(exports.rawPath[exports.tagKeyName]+'/'+t, 'utf-8');
	var js = riot.compile(code);
	fs.writeFile(exports.staticPath[exports.tagKeyName]+'/'+t.match(reg)[1]+'.'+t.match(reg)[2], js);
}
function mapize(a,b){
	for(let i=0; i<a.length;i++){
		b[a[i].match(reg)[1]] = a[i].match(reg)[0];
	}
}