/**! @license script.js; JavaScript for jsCMS by Yuki TANABE on Oct 26, 2016 | pm8.jp */

//config
const adminPath='/admin';
const fragmentsDir ='/lib/fragments';
const jsonUrl = '/lib/articles_tmp.json';
const loopFileName = 'article.txt';
const listIdName = '[data-ctl="list"]';
const loopTitleMark = '___TITLE___';
const loopBodyMark = '___BODY___';
const loopTimeMark = '___TIME___';
const loopIdMark = '___ID___';
const loopUpdateMark = '___UPDATE___';
const jsonTitleKey = 'title';
const jsonBodyKey = 'body';
const jsonTimeKey = 'time';
const jsonUpdateKey = 'update';
const jsonIdKey = '_id';

var tmp = '';
var result='';

// List article from article.json
fetch(jsonUrl,{
	method:'GET'
}).then(function(res){
	return res.json();
}).then(function(json){
	fetch(fragmentsDir+'/'+loopFileName,{method:'GET'}).then(function (res) {
		return res.text();
	}).then(function(h){
		tmp=h;
		const list = document.querySelectorAll(listIdName)[0];
		for(var i = json.length-1; 0<=i; i--) {
			var time=new Date(json[i][jsonTimeKey]);
			var update=new Date(json[i][jsonUpdateKey]);
			var timeStr=' ';
			timeStr += time.getFullYear()+'/';
			if(time.getMonth()+1<10) timeStr += '0';
			timeStr += time.getMonth()+1;
			if(time.getDate()+1<10) timeStr += '0';
			timeStr += '/'+time.getDate()+' ';
			timeStr += time.getHours()+':'+time.getMinutes()+':'+time.getSeconds();
			var updateStr='';
			updateStr += time.getFullYear()+'/';
			updateStr += time.getMonth()+1;
			updateStr += '/'+time.getDate()+' ';
			updateStr += time.getHours()+':'+time.getMinutes()+':'+time.getSeconds();
			result = tmp;
			result = result.replace(loopTitleMark, json[i][jsonTitleKey]);
			result = result.replace(loopBodyMark, json[i][jsonBodyKey]);
			result = result.replace(loopTimeMark, timeStr);
			result = result.replace(loopUpdateMark, updateStr);
			if(location.pathname===adminPath){
				result = result.replace(loopIdMark, json[i][jsonIdKey]);
			}else{
				result = result.replace(loopIdMark, i);
			}
			list.innerHTML+=result;
		}
	});
});